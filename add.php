<?php
 
require_once 'init.php';
 
// pega os dados do formuário
$equipe = isset($_POST['equipe']) ? $_POST['equipe'] : null;
$tarefa = isset($_POST['tarefa']) ? $_POST['tarefa'] : null;
$pontuacao = isset($_POST['pontuacao']) ? $_POST['pontuacao'] : null;
$obs = isset($_POST['obs']) ? $_POST['obs'] : null;
 
 
// validação (bem simples, só pra evitar dados vazios)
if (empty($equipe) || empty($tarefa) || empty($pontuacao) || empty($obs))
{
    echo "<script>alert('Preencha todos os campos');location.href=\"comissao.php\";</script>";
    exit;
}
 
// insere no banco
$PDO = db_connect();
$sql = "INSERT INTO dados(equipe, tarefa, pontuacao, obs) VALUES(:equipe, :tarefa, :pontuacao, :obs)";
$stmt = $PDO->prepare($sql);
$stmt->bindParam(':equipe',$equipe);
$stmt->bindParam(':tarefa', $tarefa);
$stmt->bindParam(':pontuacao', $pontuacao);
$stmt->bindParam(':obs', $obs);

if ($stmt->execute())
{
	 echo "<script>alert('Pontuação cadastrada com sucesso !');location.href=\"comissao.php\";</script>";
   
}
else
{
    echo "Erro ao cadastrar";
    print_r($stmt->errorInfo());
}