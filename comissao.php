<?php
require 'init.php';

?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="shortcut icon" href="img/favicon.ico">
	<link rel="js" href="img/favicon.ico">


	<script type="text/javascript">
 		function startTime() {
     		var today=new Date();
    		var h=today.getHours();
   			var m=today.getMinutes();
     		var s=today.getSeconds();
     
     		m=checkTime(m);
     		s=checkTime(s);
     		document.getElementById('txt').innerHTML=h+":"+m+":"+s;
     		t=setTimeout('startTime()',500);
 		}

 		function checkTime(i){
 			if (i<10) {
     		i="0" + i;
			}
     		return i;
 		}
 </script>

<title> I Gincana Estudantil 
</title>
</head>
<body onload="startTime()">
	<center><div id="box">
        <img class="borda" src="img/logo.jpg">
        <div class="botoes">
            <a href="index.php"><div id="home">HOME</div></a>
            <a href="tarefas.html"><div id="tarefas">TAREFAS</div></a>
            <a href="comissao.php"><div id="comissao">PONTUAÇÃO</div></a>
            <a href="relatorio.php"><div id="comissao">RELATÓRIOS</div></a>
        </div>
        </br>
        </br>
         </br>
        </br>
        <table class="tabela" >
        <tr>
            <td>
                
                <table class="tabform">
                                    
                                <form action="add.php" method="post">
                                    <tr>
                                        <div >
                                        <td class="tdtab">
                                            
                                   
                                        <label for="equipe">Equipe:</label>
                                        </td>
                                        <td >
                                       <select class="select" name="equipe">
                                        <option> </option>
                                        <option value="1">Lendários</option>
                                        <option value="2">The Monsters</option>
                                        </select>
                                        </td>
                                    </div>
                                </tr>
                                <tr>

                                    <div>
                                        <td class="tdtab">
                                        <label for="tarefa">Tarefa:</label>
                                    </td>
                                    <td>
                                       <select class="select" name="tarefa">
                                        <option> </option>
                                        <option value="1">MAJESTADES RECICLÁVEIS</option>
                                        <option value="2">IDENTIDADE GINCANEIRA</option>
                                        <option value="3">O SHOW VAI COMEÇAR</option>
                                        <option value="4">NO ESTILO POETISA</option>
                                        <option value="5">CORAL DA ABIGAIL</option>
                                        <option value="6">ASTROS DE CINEMA I</option>
                                        <option value="7">ASTROS DE CINEMA II</option>
                                        <option value="8">ASTROS DE CINEMA III</option>
                                        <option value="9">ASTROS DE CINEMA IV</option>
                                        <option value="10">ASTROS DE CINEMA V</option>
                                        <option value="11">"SEQUENCIA DE ROMA..."</option>
                                        <option value="12">“TODO MUNDO USA...”</option>
                                        <option value="13">‘SPEAK ENGLISH...”</option>
                                        <option value="14">"SMARTPHONES”</option>
                                        <option value="15">“COMISSÃO DE PROVAS”</option>
                                        <option value="16">“A IDÉIA SAINDO DO PAPEL....”</option>
                                        <option value="17">“PLAYLIST”</option>
                                        <option value="18">“BELINGUE”</option>
                                        <option value="19">“NÃO “CORRIGA” O MEU JOGO”</option>
                                        <option value="20">“PRECISAM DESCANÇAR...”</option>
                                        <option value="21">SE LIGA..</option>
                                        <option value="22">“GINCANA ESTUDANTIL”</option>
                                        <option value="23">“VAI E VOLTA”</option>
                                        <option value="24">COTIDIANO TEATRAL</option>
                                        <option value="25">COM JEITINHO II</option>
                                        <option value="26">CORRIDA MALUCA</option>
                                        <option value="27">PAVIO CURTO</option>
                                        <option value="28">PULMÃO DE AÇO</option>
                                        <option value="29">COM JEITINHO I</option>
                                        <option value="32">TRIO DA TORRE</option>
                                        <option value="33">LARANJA...</option>
                                        <option value="34">QUARTETO FANTÁSTICO</option>
                                        <option value="40">MISTÉRIOS DA ABIGAIL SAMPAIO III</option>
                                        <option value="41">MISTÉRIOS DA ABIGAIL SAMPAIO II</option>
                                        <option value="42">MISTÉRIOS DA ABIGAIL SAMPAIO I</option>
                                        <option value="43">MISTÉRIOS DA ABIGAIL SAMPAIO IV</option>
                                        <option value="44">MISTÉRIOS DA ABIGAIL SAMPAIO V</option>
                                        <option value="45">SELFIE ESTUDANTIL I</option>
                                        <option value="46">SELFIE ESTUDANTIL II</option>
                                        <option value="47">SELFIE ESTUDANTIL III</option>
                                        <option value="48">SELFIE ESTUDANTIL IV</option>
                                        <option value="49">SELFIE ESTUDANTIL V</option>
                                        <option value="50">SELFIE ESTUDANTIL VI</option>
                                        <option value="72">CLICK DA PARADA III</option>
                                        <option value="73">CLICK DA PARADA IV</option>
                                        <option value="100">“MANEQUIM”</option>
                                    </select>
                                </td>
                                    </div>
                                </tr>
                                <tr>
                                    <div>
                                        <td>
                                        <label for="pontuacao">Pontuação:</label>
                                    </td>
                                    <td>
                                      <input name="pontuacao"></input>
                                    </td>
                                    </div>
                                    <div>
                                        <td>
                                        <label for="obs">Observações: </label>
                                    </td>
                                    <td>
                                      <input type="text" name="obs"></input>
                                    </td>
                                    </div>
                            </tr>
                                <div>
                                <tr><td colspan= "2"><center><input type="submit" value="Cadastrar"></input></center></td></tr>
                                
                                </div>
                            </form>

                            
                        </div>
                        </td>

                    </tr>
               
                </table>
            </td>
            <td class="colunlateral">
                <center>
                    <h1>HORÁRIO OFICIAL
                <div id="txt"></div>
                <BR>
                    COMISSÃO DE PROVAS</center>
                <img class="imgcp" src="img/cp.png">
                <br>
                <br>
                <br>
              
            </td>
        </tr>
        </table> 
       
    </div>
        </br>
    </div>



    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/jquery.backstretch.min.js"></script>
    <script src="js/scripts.js"></script> 
</body>
</html>